#!/usr/bin/env python

from distutils.core import setup

setup(name = 'Sunrise Python Utilities',
      version = '0.1',
      description = 'Misc python scripts used in Sunrise projects',
      author = 'Sunrise Games',
      author_email = 'support@sunrise.games',
      url = 'https://gitlab.com/sunrisemmos/libsunrise',
      packages = ['libsunrise']
)